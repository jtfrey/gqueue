#
# gqueue
# Next-generation job script templating
#
# __init__
# Initialize the library namespace.
#
# Copyright (c) 2019
# Dr. J Frey, University of Delaware
#

from Scriptlet import Scriptlet
from Resource import *
from Scheduler import *
from Application import *

__version__ = '3.0.3'

def jobScriptWithResources(scheduler, application, resources):
    if resources is None:
        resources = {}
    try:
        templateString = application.jobScriptTemplateForSchedulerAndResources(scheduler, resources)
        searchFrom = 0
        while ( True ):
            # Where's the next code block start?
            codeStart = templateString.find('[{%', searchFrom)
            if codeStart < 0:
                break

            # Where's the code block stop?
            codeEnd = templateString.find('%}]', codeStart)
            if codeEnd < 0:
                raise RuntimeError('unterminated code block in template')

            # Extract the code:
            code = templateString[codeStart + 3:codeEnd].strip()
            wholeLine = False
            if (codeStart == 0) or (templateString[codeStart - 1] == "\n"):
                # If this line was the first line OR the code block was preceded by a <NL>,
                # then we're possibly looking at a whole-line block.
                #
                # If the only characters following the code block are whitespace terminated by
                # a <NL>, then indicate as much and adjust the codeEnd index to be past that
                # terminal <NL> character.
                i = codeEnd + 3
                while i < len(templateString) and (templateString[i] <> "\n" and templateString[i].isspace()):
                    i = i + 1
                if templateString[i] == "\n":
                    codeEnd = i + 1
                    wholeLine = True
                else:
                    codeEnd = codeEnd + 3
            else:
                codeEnd = codeEnd + 3

            # Execute it:
            s = Scriptlet(code, { 'SCHEDULER': scheduler, 'RESOURCES': resources, 'APPLICATION': application }, {})
            if not s.execute():
                raise RuntimeError('unable to execute code block (' + s.output() + '): ' + code)
            replacement = s.output()
            if not wholeLine:
                replacement = replacement.strip()

            # Replace the code:
            searchFrom = codeStart + len(replacement)
            if wholeLine:
                # If we're replacing a whole line, then an empty replacement sees the line being
                # removed entirely.  For a non-empty replacement, add the text plus a terminal <NL>
                # character:
                if replacement:
                    templateString = templateString[:codeStart] + replacement + templateString[codeEnd:]
                else:
                    templateString = templateString[:codeStart] + templateString[codeEnd:]
            else:
                # Simple replacment of an inline (not whole-line) code block:
                templateString = templateString[:codeStart] + replacement + templateString[codeEnd:]

        # Is there an application footer to append?
        text = application.jobScriptStaticFooter(resources) if application else None
        if text:
            templateString = templateString + ("" if text.endswith("\n") else "\n") + text

        # Is there a scheduler footer to append?
        text = scheduler.jobScriptStaticFooter(resources)
        if text:
            templateString = templateString + ("" if text.endswith("\n") else "\n") + text

        # In preparation for headers, skip the hash-bang line if present:
        insertAt = 0
        if templateString.startswith("#!"):
            insertAt = templateString.find("\n", 2) + 1

        # Is there an application header to insert?
        text = application.jobScriptStaticHeader(resources) if application else None
        if text:
            templateString = templateString[:insertAt] + text + ("" if text.endswith("\n") else "\n") + templateString[insertAt:]

        # Is there a scheduler header to insert?
        text = scheduler.jobScriptStaticHeader(resources)
        if text:
            templateString = templateString[:insertAt] + text + ("" if text.endswith("\n") else "\n") + templateString[insertAt:]

        # All done:
        return templateString
    except Exception as E:
        raise RuntimeError('unable to generate job script template: ' + str(E))
