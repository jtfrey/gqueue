#
# gqueue
# Next-generation job script templating
#
# Scheduler
# Abstract base class for schedulers we can work with
#
# Copyright (c) 2019
# Dr. J Frey, University of Delaware
#

from ..ResourceProcessor import *

class Scheduler(ResourceProcessor):

    RESOURCE_TYPES = dict(ResourceProcessor.RESOURCE_TYPES.items() + {
                        'nnode': IntegerResource,
                        'ncpu': IntegerResource,
                        'ncpu-per-node': IntegerResource,
                        'mem': MemoryResource,
                        'mem-per-cpu': MemoryResource,
                        'time': TimeResource,
                        'jobname': StringResource
                     }.items())

    RESOURCE_HELP = dict(ResourceProcessor.RESOURCE_HELP.items() + {
                        'nnode': 'number of nodes to request',
                        'ncpu': 'number of cpus to request',
                        'ncpu-per-node': 'number of cpus to request per node',
                        'mem': 'memory to request per node',
                        'mem-per-cpu': 'memory to request per cpu',
                        'time': 'maximum walltime to request',
                        'jobname': 'descriptive name to associate with the job'
                      }.items())

    RESOURCE_METAVARS = dict(ResourceProcessor.RESOURCE_METAVARS.items() + {
                        'nnode': '#',
                        'ncpu': '#',
                        'ncpu-per-node': '#',
                        'mem': '#{GB|MB|KB|GiB|MiB|KiB}',
                        'mem-per-cpu': '#{GB|MB|KB|GiB|MiB|KiB}',
                        'time': '{D-}HH:MM:SS',
                        'jobname': '<name>'
                      }.items())

    @classmethod
    def cliArgumentGroupName(cls):
        return 'requestable resource options'

    def __init__(self, defaultResourcesDict = None):
        super(Scheduler, self).__init__(defaultResourcesDict)
        self._extraArgs = []

    def addCliArguments(self, cliArgParse):
        cliArgParse.add_argument(
                '--extra-argument', '-a',
                action='append',
                metavar='<arg>',
                help='specify additional arguments that will be passed when the job is submitted; can be used multiple times, words will be presented in the same order they are specified'
            )
        super(Scheduler, self).addCliArguments(cliArgParse)

    def parseCliArguments(self, cliArgs, resources = None):
        if cliArgs.extra_argument is not None:
            self._extraArgs.extend(cliArgs.extra_argument)
        return super(Scheduler, self).parseCliArguments(cliArgs, resources)

    def postFilterResources(self, resources):
        # Check for overlap between the CPU/node count resources:
        if 'ncpu' in resources:
            if 'ncpu-per-node' in resources:
                if 'nnode' in resources:
                    if resources['nnode'].value() * resources['ncpu-per-node'].value() <> resources['ncpu'].value():
                        raise ValueError('invalid combination of arguments: (number of nodes) x (ncpu per node) <> ncpu')
        return resources

    def submit(self, scriptFile, resources):
        raise NotImplemented('Scheduler.submit must be overridden by concrete subclasses')
