#
# gqueue
# Next-generation job script templating
#
# __init__
# Initialize the library namespace.
#
# Copyright (c) 2019
# Dr. J Frey, University of Delaware
#

import importlib

def createScheduler(name, defaultResourcesDict = None):
    name = name + 'Scheduler'
    try:
        scheduler = importlib.import_module('gqueue.Scheduler.' + name)
        return getattr(scheduler, name)(defaultResourcesDict)
    except Exception as E:
        raise RuntimeError('unable to load scheduler {}: {}'.format(name, str(E)))

