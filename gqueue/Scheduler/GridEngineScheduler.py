#
# gqueue
# Next-generation job script templating
#
# GridEngineScheduler
# Concrete Scheduler subclass that handles operations w.r.t. Grid Engine
#
# Copyright (c) 2019
# Dr. J Frey, University of Delaware
#

from Scheduler import *
import subprocess

class GridEngineScheduler(Scheduler):

    RESOURCE_TYPES = dict(Scheduler.RESOURCE_TYPES.items() + {
                        'queue': StringResource,
                        'pe-name': StringResource
                      }.items())

    RESOURCE_HELP = dict(Scheduler.RESOURCE_HELP.items() + {
                        'queue': 'queue(s) to submit the job to',
                        'pe-name': 'named parallel environment to use'
                      }.items())

    RESOURCE_METAVARS = dict(Scheduler.RESOURCE_METAVARS.items() + {
                        'queue': '<name>{,<name>...}',
                        'pe-name': '<pe-name>'
                      }.items())

    @classmethod
    def nameString(cls):
        return 'GridEngine'

    @classmethod
    def descriptionString(cls):
        return '(Univa|Sun) Grid Engine'

    def postFilterResources(self, resources):
        #
        # Since Grid Engine goes by slots, nnode/ncpu-per-node is a useless
        # property that we can eliminate:
        #
        if 'ncpu' not in resources:
            if 'ncpu-per-node' in resources:
                n = resources['ncpu-per-node'].value()
                if 'nnode' in resources:
                    n = n * resources['nnode'].value()
                resources['ncpu'] = IntegerValue(n)
        resources.pop('nnode', None)
        resources.pop('ncpu-per-node', None)
        return resources

    def submit(self, scriptFile, resources):
        #
        # Submit the job.  The only parameters we'll specify on the command line
        # are the job name, queue(s), and the max runtime.
        #
        cmd = ['qsub']
        if 'queue' in resources:
            cmd.append('-q')
            cmd.append(resources['queue'].value())
        if 'time' in resources:
            cmd.append('-l')
            cmd.append('h_rt={:.0f}'.format(resources['time'].value()))
        if 'jobname' in resources:
            cmd.append('-N')
            cmd.append(resources['jobname'].value())
        cmd.extend(self._extraArgs)
        cmd.append(scriptFile)

        #
        # Execute:
        #
        try:
            submission = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=True)
            (stdout, stderr) = submission.communicate()
            if submission.returncode != 0:
                raise RuntimeError(stderr)
            return stdout
        except Exception as E:
            raise RuntimeError('unable to submit job: ' + str(E))
