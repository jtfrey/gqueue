#
# gqueue
# Next-generation job script templating
#
# SlurmScheduler
# Concrete Scheduler subclass that handles operations w.r.t. Slurm
#
# Copyright (c) 2019
# Dr. J Frey, University of Delaware
#

from Scheduler import *
import subprocess

class SlurmScheduler(Scheduler):

    RESOURCE_TYPES = dict(Scheduler.RESOURCE_TYPES.items() + {
                        'partition': StringResource
                      }.items())

    RESOURCE_HELP = dict(Scheduler.RESOURCE_HELP.items() + {
                        'partition': 'partition(s) to submit the job to'
                      }.items())

    RESOURCE_METAVARS = dict(Scheduler.RESOURCE_METAVARS.items() + {
                        'partition': '<name>{,<name>...}'
                      }.items())

    @classmethod
    def nameString(cls):
        return 'Slurm'

    @classmethod
    def descriptionString(cls):
        return 'SLURM (Simple Linux Utility for Resource Management)'

    def submit(self, scriptFile, resources):
        #
        # Submit the job.  The only parameters we'll specify on the command line
        # are the job name and the max runtime.
        #
        cmd = ['sbatch']
        if 'partition' in resources:
            cmd.append('--partition=' + resources['partition'].value())
        if 'time' in resources:
            cmd.append('--time=' + resources['time'].toStringWithFormat('%d-%H:%M:%S'))
        if 'jobname' in resources:
            cmd.append('--job-name=' + resources['jobname'].value())
        cmd.extend(self._extraArgs)
        cmd.append(scriptFile)

        #
        # Execute:
        #
        try:
            submission = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=True)
            (stdout, stderr) = submission.communicate()
            if submission.returncode != 0:
                raise RuntimeError(stderr)
            return stdout
        except Exception as E:
            raise RuntimeError('unable to submit job: ' + str(E))
