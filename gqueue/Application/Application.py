#
# gqueue
# Next-generation job script templating
#
# Application
# Abstract base class for applications we can work with
#
# Copyright (c) 2019
# Dr. J Frey, University of Delaware
#

from ..ResourceProcessor import *
import os
import sys
import logging
from pkg_resources import resource_exists, resource_string

class Application(ResourceProcessor):

    _hasBeenInited = False
    _applicationSearchPaths = []
    _templateSearchPaths = []

    @classmethod
    def setupSearchPaths(cls):
        if not cls._hasBeenInited:
            if not 'GQUEUE_SYSCONFDIR' in os.environ and 'GQUEUE_PREFIX' in os.environ:
                os.environ['GQUEUE_SYSCONFDIR'] = os.path.join(os.environ['GQUEUE_PREFIX'], 'etc')
                logging.debug('set default GQUEUE_SYSCONFDIR to {}'.format(os.environ['GQUEUE_SYSCONFDIR']))

            addOnDirs = []
            if 'GQUEUE_APPLICATION_ADDONS' in os.environ:
                addOnDirs.extend(os.environ['GQUEUE_APPLICATION_ADDONS'].split(os.pathsep))
            if 'GQUEUE_SYSCONFDIR' in os.environ:
                addOnDirs.append(os.path.join(os.environ['GQUEUE_SYSCONFDIR'], 'applications'))
            else:
                addOnDirs.append('/etc/gqueue/applications')
            for d in addOnDirs:
                if os.path.isdir(d):
                    cls._applicationSearchPaths.append(d)
                    sys.path.insert(1, d)
                    logging.debug('added to Python search paths: {}'.format(d))

            tmplDirs = []
            if 'GQUEUE_TEMPLATES' in os.environ:
                tmplDirs.extend(os.environ['GQUEUE_TEMPLATES'].split(os.pathsep))
            if 'GQUEUE_SYSCONFDIR' in os.environ:
                tmplDirs.append(os.path.join(os.environ['GQUEUE_SYSCONFDIR'], 'templates'))
            else:
                tmplDirs.append('/etc/gqueue/templates')
            for d in tmplDirs:
                if os.path.isdir(d):
                    cls._templateSearchPaths.append(d)
                    logging.debug('added to template search paths: {}'.format(d))

            cls._hasBeenInited = True

    @classmethod
    def applicationSearchPaths(cls):
        cls.setupSearchPaths()
        return cls._applicationSearchPaths

    @classmethod
    def templateSearchPaths(cls):
        cls.setupSearchPaths()
        return cls._templateSearchPaths

    @classmethod
    def readTemplateFile(cls, templateFile):
        cls.setupSearchPaths()
        for d in cls._templateSearchPaths:
            p = os.path.join(d, templateFile)
            if os.path.isfile(p):
                with open(p) as f:
                    return f.read()

        # Finally, try to find it inside the module itself:
        if resource_exists('gqueue', 'Templates/' + templateFile):
            return resource_string('gqueue', 'Templates/' + templateFile)

        raise RuntimeError('template {} not found'.format(templateFile))

    @classmethod
    def nameString(cls):
        if cls.__name__.endswith('Application'):
            return cls.__name__[:-11]
        raise RuntimeError('invalid Application class name: ' + cls.__name__)

    @classmethod
    def cliArgumentFlagPrefix(cls):
        return cls.nameString().upper() + '_'

    @classmethod
    def cliArgumentGroupName(cls):
        return 'application-specific options'

    def jobScriptTemplateForSchedulerAndResources(self, scheduler = None, resources = None):
        raise NotImplemented('Application.jobScriptTemplate must be overridden by concrete subclasses')
