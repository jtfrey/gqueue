#
# gqueue
# Next-generation job script templating
#
# __init__
# Initialize the library namespace.
#
# Copyright (c) 2019
# Dr. J Frey, University of Delaware
#

import importlib
import logging
from Application import Application
from pkg_resources import resource_listdir

Application.setupSearchPaths()

def createApplication(name, defaultResourcesDict = None):
    name = name + 'Application'
    try:
        logging.debug('attempting to load external Application class ' + name)
        app = importlib.import_module(name)
        return getattr(app, name)(defaultResourcesDict)
    except ImportError as IE:
        try:
            logging.debug('attempting to load as built-in Application class ' + name)
            app = importlib.import_module('gqueue.Application.' + name)
            return getattr(app, name)(defaultResourcesDict)
        except Exception as E:
            raise RuntimeError('unable to load application {}: {}'.format(name, str(E)))
    except Exception as E:
        raise RuntimeError('unable to load application {}: {}'.format(name, str(E)))


def applicationList():
    import glob
    import os
    import re

    applications = dict()

    # Anything present in the library itself?
    for appPath in resource_listdir('gqueue', 'Application'):
        m = re.match(r'^(.+Application).py$', appPath)
        if m is not None:
            className = m.group(1)
            try:
                app = importlib.import_module('gqueue.Application.' + className)
                applications[className] = app
            except Exception as E:
                logging.info('unable to load built-in Application class %s: %s', className, str(E))


    # Check other directories:
    for appDir in reversed(Application.applicationSearchPaths()):
        for appPath in glob.iglob(os.path.join(appDir, '?*Application.py')):
            className = os.path.basename(appPath)[:-3]
            try:
                app = importlib.import_module(className)
                applications[className] = app
            except Exception as E:
                logging.info('unable to load external Application class %s: %s', className, str(E))

    return applications
