#
# gqueue
# Next-generation job script templating
#
# GaussianApplication
# Gaussian quantum chemistry suite jobs
#
# Copyright (c) 2019
# Dr. J Frey, University of Delaware
#

from Application import *
import sys
import re
import logging

class GaussianNProcLine:
    pass

class GaussianMemLine:
    pass

class GaussianApplication(Application):

    RESOURCE_TYPES = dict(Application.RESOURCE_TYPES.items() + {
                        'gaussian-version': StringResource,
                        'gaussian-logfile': StringResource,
                        'gaussian-mem-overhead': MemoryResource
                     }.items())

    RESOURCE_HELP = dict(Application.RESOURCE_HELP.items() + {
                        'gaussian-version': 'VALET version of Gaussian to use',
                        'gaussian-logfile': 'Send Gaussian stdout to this file',
                        'gaussian-mem-overhead': 'Additional memory to request (beyond the core size in the input file) to account for the in-memory size of the executables themselves'
                      }.items())

    RESOURCE_METAVARS = dict(Application.RESOURCE_METAVARS.items() + {
                        'gaussian-version': '<version-id>',
                        'gaussian-logfile': '<path>',
                        'gaussian-mem-overhead': '#{GB|MB|KB|GiB|MiB|KiB}'
                      }.items())

    @classmethod
    def descriptionString(cls):
        return 'Gaussian quantum chemistry suite'

    def addCliArguments(self, cliArgParse):
        # Get resource stuff added:
        super(GaussianApplication, self).addCliArguments(cliArgParse)

        # Add positional argument for input file:
        cliArgParse.add_argument(
                        'GAUSSIAN_inputFile',
                        metavar='<input file>',
                        nargs=1,
                        help='Gaussian input file to be submitted'
                    )

    def parseCliArguments(self, cliArgs, resources = None):
        # Get resource stuff parsed:
        resources = super(GaussianApplication, self).parseCliArguments(cliArgs, resources)

        # Overhead memory?
        if not 'gaussian-mem-overhead' in resources:
            resources['gaussian-mem-overhead'] = MemoryResource(125 * 1024 * 1024)

        # Pull-in the Gaussian input:
        if hasattr(cliArgs, 'GAUSSIAN_inputFile'):
            gaussianInputFile = cliArgs.GAUSSIAN_inputFile[0]
            try:
                if gaussianInputFile == '-':
                    gaussianInputStream = sys.stdin
                    logging.debug('reading Gaussian input from stdin')
                else:
                    gaussianInputStream = open(gaussianInputFile, 'r')
                    logging.debug('reading Gaussian input from ' + gaussianInputFile)
                inputString = gaussianInputStream.read()
                if gaussianInputFile != '-':
                    gaussianInputStream.close()
            except Exception as E:
                raise RuntimeError('unable to read Gaussian input file {}: {}'.format(gaussianInputFile, str(E)))
        else:
            raise RuntimeError('no Gaussian input file specified')

        # Process the input file, looking for nproc and mem lines and
        # replacing with stand-in tokens.  If any nproc/mem lines
        # got processed, replace the applicable resources:
        nproc = 0
        mem = -1
        self._gaussianInput = []
        nprocRegex = re.compile(r'^%\s*nproc(shared)?=(\d+)\s*$', re.IGNORECASE)
        memRegex = re.compile(r'^%\s*mem=(\d+)([gmk]?)([wb]?)\s*$', re.IGNORECASE)
        lineAccum = ''
        for line in inputString.splitlines():
            m = nprocRegex.search(line)
            if m is not None:
                thisNProc = int(m.group(2))
                logging.info('link0 processor count line found in input file, ncpu = {}'.format(thisNProc))
                if thisNProc > nproc:
                    nproc = thisNProc
                if lineAccum:
                    self._gaussianInput.append(lineAccum)
                    lineAccum = ''
                self._gaussianInput.append(GaussianNProcLine)
            else:
                m = memRegex.search(line)
                if m is not None:
                    thisMem = int(m.group(1))
                    if m.group(3).lower() == 'w':
                        thisMem = thisMem * 8
                    unit = m.group(2).lower()
                    if unit == 'g':
                        thisMem = thisMem * (1024 * 1024 * 1024)
                    if unit == 'm':
                        thisMem = thisMem * (1024 * 1024)
                    if unit == 'k':
                        thisMem = thisMem * 1024
                    logging.info('link0 memory size line found in input file, byte count = {}'.format(thisMem))
                    if thisMem > mem:
                        mem = thisMem
                    if lineAccum:
                        self._gaussianInput.append(lineAccum)
                        lineAccum = ''
                    self._gaussianInput.append(GaussianMemLine)
                else:
                    lineAccum = lineAccum + line + "\n"
        if lineAccum:
            self._gaussianInput.append(lineAccum)

        # Memory size?
        if mem >= 0:
            logging.info('noted Gaussian core memory size of {} bytes'.format(mem))
            resources['GAUSSIAN_mem'] = MemoryResource(mem)

        # Nproc?
        if nproc > 0 and 'ncpu' not in resources:
            logging.info('using processor count of {} from Gaussian input file'.format(nproc))
            resources['ncpu'] = IntegerResource(nproc)

        return resources

    def postFilterResources(self, resources):
        # Force a single host and use of ncpu:
        resources['nnode'] = IntegerResource(1)
        if 'ncpu-per-node' in resources:
            if 'ncpu' in resources:
                resources.pop('ncpu-per-node', None)
            else:
                resources['ncpu'] = resources.pop('ncpu-per-node', None)
        elif not 'ncpu' in resources:
            resources['ncpu'] = IntegerResource(1)

        # Memory stuff:
        if 'GAUSSIAN_mem' in resources:
            minimumMem = resources['GAUSSIAN_mem'].value() + resources['gaussian-mem-overhead'].value()
            if 'mem' not in resources or resources['mem'].value() < minimumMem:
                resources['mem'] = MemoryResource(minimumMem)
        elif 'mem' not in resources:
            resources['mem'] = MemoryResource(resources['gaussian-mem-overhead'].value() + 160 * 1024 * 1024)

        return resources

    def jobScriptStaticHeader(self, resources):
        return """##
## Job script wrapper generated by gqueue Gaussian application
##
"""

    def jobScriptTemplateForSchedulerAndResources(self, scheduler = None, resources = None):
        return Application.readTemplateFile('Gaussian.tmpl')

    def generateInputFile(self, resources):
        # Memory specified?
        memLine = False
        if 'GAUSSIAN_mem' in resources:
            bytes = resources['GAUSSIAN_mem'].value()
            if bytes == 0:
                # Replace with mem limit minus overhead:
                if 'mem' in resources:
                    bytes = resources['mem'].value()
                    if bytes > resources['gaussian-mem-overhead'].value():
                        mem = MemoryResource(bytes - resources['gaussian-mem-overhead'].value())
                        memLine = "% mem={}MB\n".format(mem.formattedValue('MiB', showUnit = False, digits = 0))
            else:
                memLine = "% mem={}MB\n".format(resources['GAUSSIAN_mem'].formattedValue('MiB', showUnit = False, digits = 0))
        # CPU count?
        if 'ncpu' in resources:
            cpuLine = "% nprocshared={}\n".format(resources['ncpu'].value())
        else:
            cpuLine = False

        # Reconstitute the input file:
        inputFile = ''
        for chunk in self._gaussianInput:
            if chunk == GaussianMemLine:
                if memLine:
                    inputFile = inputFile + memLine
            elif chunk == GaussianNProcLine:
                if cpuLine:
                    inputFile = inputFile + cpuLine
            else:
                # Escape any "$" characters in the chunk:
                inputFile = inputFile + chunk.replace('$', '\\$')

        return inputFile
