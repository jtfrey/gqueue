#
# gqueue
# Next-generation job script templating
#
# SimpleWrapApplication
# Wrap one or more script files into a single job script.
#
# Copyright (c) 2019
# Dr. J Frey, University of Delaware
#

from Application import *
import sys
import os
import re

class SimpleWrapApplication(Application):

    @classmethod
    def descriptionString(cls):
        return 'Wrap one or more script files'

    def addCliArguments(self, cliArgParse):
        # Get resource stuff added:
        super(SimpleWrapApplication, self).addCliArguments(cliArgParse)

        # Execute directly?
        cliArgParse.add_argument(
                        '--execute-scripts',
                        dest='SIMPLEWRAP_execute_scripts',
                        action='store_true',
                        default=False,
                        help='do not concatenate the script(s) into the generated job script, just execute them'
                    )
        cliArgParse.add_argument(
                        '--execute-wrapper',
                        metavar='<cmd/option>',
                        action='append',
                        dest='SIMPLEWRAP_execute_wrapper',
                        help='prefix execution of the scripts with this word; can be used multiple times for each argument in sequence'
                    )

        # Add positional argument for script file(s):
        cliArgParse.add_argument(
                        'SIMPLEWRAP_scriptFiles',
                        metavar='<input file>',
                        nargs='+',
                        help='Script(s) to be wrapped (in the sequence they are presented)'
                    )

    def parseCliArguments(self, cliArgs, resources = None):
        # Get resource stuff parsed:
        resources = super(SimpleWrapApplication, self).parseCliArguments(cliArgs, resources)

        # Execute or concat?
        self._shouldExecuteScriptFiles = cliArgs.SIMPLEWRAP_execute_scripts
        if self._shouldExecuteScriptFiles:
            self._executionWrapper = cliArgs.SIMPLEWRAP_execute_wrapper
        else:
            self._executionWrapper = False

        # Validate the script files:
        if hasattr(cliArgs, 'SIMPLEWRAP_scriptFiles'):
            self._scriptFiles = []
            for scriptFile in cliArgs.SIMPLEWRAP_scriptFiles:
                try:
                    f = open(scriptFile, 'r')
                    f.close()
                    self._scriptFiles.append(scriptFile)
                except:
                    raise RuntimeError('script file not readable: ' + scriptFile)
        else:
            raise RuntimeError('no script file(s) specified')
        return resources

    def jobScriptTemplateForSchedulerAndResources(self, scheduler = None, resources = None):
        return Application.readTemplateFile('SimpleWrap.tmpl')

    def concatInputScripts(self, resources):
        scriptContent = ''
        if self._shouldExecuteScriptFiles:
            for scriptFile in self._scriptFiles:
                if not os.path.isabs(scriptFile) and not scriptFile.startswith('./') and not scriptFile.startswith('../'):
                    scriptFile = './' + scriptFile
                if self._executionWrapper:
                    for word in self._executionWrapper:
                        scriptContent = scriptContent + word + " \\\n    "
                scriptContent = scriptContent + scriptFile + "\n"
            scriptContent = scriptContent + "\n"
        else:
            hashBangRegex = re.compile(r'#!.*')
            for scriptFile in self._scriptFiles:
                scriptContent = scriptContent + "####\n#### copied from {}\n####\n".format(os.path.realpath(scriptFile))
                with open(scriptFile, 'r') as f:
                    for line in f:
                        if not hashBangRegex.match(line):
                            scriptContent = scriptContent + line
                scriptContent = scriptContent + "\n"
        return scriptContent
