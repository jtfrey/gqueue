#
# gqueue
# Next-generation job script templating
#
# Resource
# A class cluster used to represent requestable resources.
#
# Copyright (c) 2019
# Dr. J Frey, University of Delaware
#

import re
import datetime
import time
import math
from decimal import Decimal

class Resource(object):

    @classmethod
    def typeOfValue(cls):
        raise NotImplementedError('Resource is an abstract base class')

    @classmethod
    def coerceValue(cls, value):
        raise NotImplementedError('Resource is an abstract base class')

    def __init__(self, value = None):
        try:
            self._value = self.__class__.coerceValue(value)
        except Exception as E:
            raise ValueError('{} expects a value of type {}: {}'.format(self.__class__.__name__, self.__class__.typeOfValue(), str(E)))

    def __str__(self):
        return str(self._value)

    def __repr__(self):
        return '{}({})'.format(self.__class__.__name__, repr(self._value))

    def value(self):
        return self._value


class StringResource(Resource):

    @classmethod
    def typeOfValue(cls):
        return 'string'

    @classmethod
    def coerceValue(cls, value):
        if isinstance(value, basestring) or isinstance(value, str):
            return value
        return str(value)


class IntegerResource(Resource):

    @classmethod
    def typeOfValue(cls):
        return 'integer'

    @classmethod
    def coerceValue(cls, value):
        return int(value)


class BooleanResource(Resource):

    @classmethod
    def typeOfValue(cls):
        return 'boolean'

    @classmethod
    def coerceValue(cls, value):
        if isinstance(value, basestring) or isinstance(value, str):
            value = value.lower()
        if value in (True, 'true', 't', 'yes', 'y'):
            return True
        if value in (False, 'false', 'f', 'no', 'n'):
            return False
        if int(value) == 0:
            return False
        if int(value) != 0:
            return True
        raise ValueError('not a valid boolean value: ' + str(value))


class FloatResource(Resource):

    @classmethod
    def typeOfValue(cls):
        return 'float'

    @classmethod
    def coerceValue(cls, value):
        return float(value)

    def __init__(self, value = None, precision = -1):
        super(FloatResource,self).__init__(value)
        self.setPrecision(precision)

    def __str__(self):
        if self._precision < 0:
            fltFormat = '{:f}'
        else:
            fltFormat = '{:.' + str(self._precision) + 'f}'
        return fltFormat.format(self.value())

    def __repr__(self):
        return '{}({}, precision={})'.format(self.__class__.__name__, repr(self._value), repr(self._precision))

    def precision(self):
        return self._precision

    def setPrecision(self, precision):
        self._precision = int(precision)


class TimeResource(FloatResource):

    @classmethod
    def typeOfValue(cls):
        return 'time'

    @classmethod
    def coerceValue(cls, value):
        if isinstance(value, basestring) or isinstance(value, str):
            seconds = -1

            # Try the hyphen- and colon-delimited format:
            m = re.match(r'^\s*(([0-9]+)-)?([0-9]+)(:([0-9]+)(:([0-9]+))?)?\s*$', value)
            if m is not None:
                if m.group(2):
                    seconds = 86400.0 * float(m.group(2))
                if m.group(7):
                    seconds = (0 if (seconds < 0) else seconds) + 3600.0 * float(m.group(3)) + 60.0 * float(m.group(5)) + float(m.group(7))
                elif m.group(5):
                    seconds = (0 if (seconds < 0) else seconds) + 3600.0 * float(m.group(3)) + 60.0 * float(m.group(5))
                elif m.group(3):
                    seconds = (0 if (seconds < 0) else seconds) + 3600.0 * float(m.group(3))

            if seconds < 0:
                # Try descriptive textual formats:
                m = re.search(r'([0-9]+(\.[0-9]*)?)\s*s(ec(onds?|s)?)?', value)
                if m is not None:
                    seconds = float(m.group(1))
                m = re.search(r'([0-9]+(\.[0-9]*)?)\s*m(in(utes?|s)?)?', value)
                if m is not None:
                    seconds = (0 if (seconds < 0) else seconds) + 60.0 * float(m.group(1))
                m = re.search(r'([0-9]+(\.[0-9]*)?)\s*h(ours?|rs?)?', value)
                if m is not None:
                    seconds = (0 if (seconds < 0) else seconds) + 3600.0 * float(m.group(1))
                m = re.search(r'([0-9]+(\.[0-9]*)?)\s*d(ays?)?', value)
                if m is not None:
                    seconds = (0 if (seconds < 0) else seconds) + 86400.0 * float(m.group(1))

            if seconds < 0:
                raise ValueError('invalid time string: {}'.format(value))
            return seconds
        else:
            return float(value)

    def __str__(self):
        seconds = self.value()
        days = math.floor(seconds / 86400.0)
        seconds = seconds - (days * 86400.0)
        hours = math.floor(seconds / 3600.0)
        seconds = seconds - (hours * 3600.0)
        minutes = math.floor(seconds / 60.0)
        seconds = seconds - (minutes * 60.0)
        return '{:.0f}-{:02.0f}:{:02.0f}:{:02.0f}'.format(days, hours, minutes, seconds)

    def __repr__(self):
        return '{}({})'.format(self.__class__.__name__, repr(str(self)))

    def toStringWithFormat(self, strftimeFormat):
        seconds = self.value()
        days = math.floor(seconds / 86400.0)
        seconds = seconds - (days * 86400.0)
        hours = math.floor(seconds / 3600.0)
        seconds = seconds - (hours * 3600.0)
        minutes = math.floor(seconds / 60.0)
        seconds = seconds - (minutes * 60.0)

        out = strftimeFormat.replace('%S', '{:02.0f}'.format(seconds))
        out = out.replace('%M', '{:02.0f}'.format(minutes))
        out = out.replace('%H', '{:02.0f}'.format(hours))
        out = out.replace('%d', '{:.0f}'.format(days))

        return out


class MemoryResource(Resource):

    @staticmethod
    def multiplier(unit):
        powerSet = 'kmgtpezy'
        m = re.match(r'\s*([YZEPTGMKyzeptgmk])?([Ii])?[Bb]?', unit)
        if m is None:
            raise RuntimeError('invalid memory unit: {}'.format(unit))
        base = 1024 if m.group(2) else 1000
        power = (1 + powerSet.index(m.group(1).lower())) if m.group(1) else 0
        return Decimal(base ** power)

    @staticmethod
    def normalizeUnitStr(unit):
        m = re.match(r'\s*([YZEPTGMKyzeptgmk])?([Ii])?[Bb]?', unit)
        if m is None:
            raise RuntimeError('invalid memory unit: {}'.format(unit))
        if not m.group(2) and (m.group(1) == 'k' or m.group(1) == 'K'):
            return 'kB'
        return m.group(1).upper() + m.group(2).lower() + 'B'

    @classmethod
    def typeOfValue(cls):
        return 'memory'

    @classmethod
    def coerceValue(cls, value):
        if isinstance(value, basestring) or isinstance(value, str):
            m = re.match(r'\s*([0-9]+(\.[0-9]*)?)\s*(.*)', value)
            if m is not None:
                return Decimal(m.group(1)) * MemoryResource.multiplier(m.group(3))
            else:
                raise ValueError('{} is not a valid memory value'.format(value))
        else:
            return Decimal(value)

    def formattedValue(self, unit = 'B', showUnit = True, digits = 1):
        value = self.value() / MemoryResource.multiplier(unit)
        if showUnit:
            return ('{:.' + str(digits) + 'f} {}').format(value, MemoryResource.normalizeUnitStr(unit))
        else:
            return  ('{:.' + str(digits) + 'f}').format(value)

    def naturalStringValue(self, useDigitalUnits = True):
        power = -1
        if useDigitalUnits:
            base = Decimal(1024)
            digitalMark = 'i'
            powerSet = 'KMGTPEZY'
        else:
            base = Decimal(1000)
            digitalMark = ''
            powerSet = 'kMGTPEZY'
        value = self.value()
        while ( power < len(powerSet) and value > base ):
            value = value / base
            power = power + 1
        if power >= 0:
            unit = powerSet[power] + digitalMark + 'B'
            digits = '1'
        else:
            unit = 'B'
            digits = '0'
        return ('{:.' + digits + 'f} {}').format(value, unit)



if __name__ == '__main__':
    s = StringResource(4)
    print s
    print repr(s)

    i = IntegerResource('45')
    print i
    print repr(i)

    f = FloatResource('3.14159', precision=4)
    print f
    print repr(f)

    m = MemoryResource('4.5 MiB')
    print m
    print repr(m)

    m = MemoryResource('1G')
    print m
    print repr(m)

    m = MemoryResource('4.1 Yib')
    print m
    print repr(m)
    print m.formattedValue('MiB', showUnit = False, digits = 0) + 'M'
    print m.naturalStringValue(False)

    t = TimeResource('1h1m')
    print t
    print repr(t)
