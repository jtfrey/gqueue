#
# gqueue
# Next-generation job script templating
#
# Scriptlet
# Execute a chunk of Python code in an isolated runtime.
#
# Copyright (c) 2019
# Dr. J Frey, University of Delaware
#

import sys
import StringIO
import contextlib

@contextlib.contextmanager
def stdoutIO(stdout=None):
    old = sys.stdout
    if stdout is None:
        stdout = StringIO.StringIO()
    sys.stdout = stdout
    yield stdout
    sys.stdout = old

class Scriptlet(object):

    def __init__(self, codeString, globalVars = {}, localVars = {}):
        self._codeString = codeString
        self._globalVars = globalVars
        self._localVars = localVars
        self._output = None
        self._hasBeenExecuted = False
        self._didProduceError = False

    def execute(self):
        self._hasBeenExecuted = True
        self._didProduceError = False
        self._output = None
        try:
            with stdoutIO() as s:
                exec(self._codeString, self._globalVars, self._localVars)
            self._output = s.getvalue()
        except Exception as E:
            self._output = str(E)
            self._didProduceError = True
        return not self._didProduceError

    def globalVars(self):
        return self._globalVars.viewitems()

    def localVars(self):
        return self._localVars.viewitems()

    def hasBeenExecuted(self):
        return self._hasBeenExecuted

    def didProduceError(self):
        return self._didProduceError

    def output(self):
        return self._output

