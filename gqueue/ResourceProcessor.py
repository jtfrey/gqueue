#
# gqueue
# Next-generation job script templating
#
# ResourceProcessor
# Abstract base class for an object that defines and parses
# resources.
#
# Copyright (c) 2019
# Dr. J Frey, University of Delaware
#

from Resource import *

class ResourceProcessor(object):

    RESOURCE_TYPES = {}
    RESOURCE_HELP = {}
    RESOURCE_METAVARS = {}

    @classmethod
    def nameString(cls):
        raise NotImplementedError('ResourceProcessor.nameString must be overridden by concrete subclasses')

    @classmethod
    def descriptionString(cls):
        raise NotImplementedError('ResourceProcessor.descriptionString must be overridden by concrete subclasses')

    @classmethod
    def cliArgumentGroupName(cls):
        raise NotImplementedError('ResourceProcessor.cliArgumentGroupName must be overridden by concrete subclasses')

    @classmethod
    def cliArgumentFlagPrefix(cls):
        return ''

    @classmethod
    def isValidResourceKey(cls, resourceKey):
        return resourceKey in cls.RESOURCE_TYPES

    def __init__(self, defaultResourcesDict = None):
        self._defaultResources = dict()
        if defaultResourcesDict:
            for k, v in defaultResourcesDict.iteritems():
                if self.__class__.isValidResourceKey(k):
                    self._defaultResources[k] = self.createResourceWithKey(k, v)

    def createResourceWithKey(self, resourceKey, valueString):
        if resourceKey in self.__class__.RESOURCE_TYPES:
            return (self.__class__.RESOURCE_TYPES[resourceKey])(valueString)
        raise ValueError('undefined resource key: {}', resourceKey)

    def defaultResources(self):
        return self._defaultResources

    def preFilterResources(self, resources):
        return resources

    def postFilterResources(self, resources):
        return resources

    def addCliArguments(self, cliArgParse):
        k = self.__class__.RESOURCE_TYPES.keys()
        if len(k):
            k.sort()
            argGroup = cliArgParse.add_argument_group(self.__class__.cliArgumentGroupName())
            for rsrcName in k:
                if rsrcName in self.__class__.RESOURCE_METAVARS:
                    argGroup.add_argument(
                            '--' + rsrcName,
                            metavar=self.__class__.RESOURCE_METAVARS[rsrcName],
                            dest= self.__class__.cliArgumentFlagPrefix() + rsrcName,
                            help=self.__class__.RESOURCE_HELP[rsrcName]
                        )
                else:
                    argGroup.add_argument(
                            '--' + rsrcName,
                            dest=__class__.cliArgumentFlagPrefix() + rsrcName,
                            help=self.__class__.RESOURCE_HELP[rsrcName]
                        )

    def parseCliArguments(self, cliArgs, resources = None):
        if resources is None:
            resources = dict()
        for rsrcKey, rsrcClass in self.__class__.RESOURCE_TYPES.iteritems():
            cliArgKey = self.__class__.cliArgumentFlagPrefix() + rsrcKey
            if hasattr(cliArgs, cliArgKey) and getattr(cliArgs, cliArgKey):
                resources[rsrcKey] = rsrcClass(getattr(cliArgs, cliArgKey))
        return resources

    def jobScriptStaticHeader(self, resources):
        return None

    def jobScriptStaticFooter(self, resources):
        return None
