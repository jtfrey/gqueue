#!/usr/bin/env python
#
# Installation script for gqueue
#

from setuptools import setup, find_packages

setup(
    name="gqueue",
    version="3.0.3",
    packages=find_packages(),
    package_data={'gqueue': ['Templates/*.tmpl']},
    scripts=['bin/gqueue'],
    zip_safe=True,

    author='Jeffrey Frey',
    author_email="frey@udel.edu",
    description="A batch job scripting template library",
)
