#
# gqueue
# Next-generation job script templating
#
# NewApplication
# A template for an external (outside the module) Application subclass
# for use with gqueue.
#
# Copyright (c) 2019
# Dr. J Frey, University of Delaware
#

from gqueue.Resource import *
from gqueue.Application import Application


class NewApplication(Application):

    RESOURCE_TYPES = dict(Application.Application.RESOURCE_TYPES.items() + {
                        ...new resources...
                     }.items())

    RESOURCE_HELP = dict(Application.Application.RESOURCE_HELP.items() + {
                        ...new resources...
                      }.items())

    RESOURCE_METAVARS = dict(Application.Application.RESOURCE_METAVARS.items() + {
                        ...new resources...
                      }.items())

    @classmethod
    def descriptionString(cls):
        return 'brief description of what kind of job this class handles'

        :
    other overridden methods, etc.
        :
