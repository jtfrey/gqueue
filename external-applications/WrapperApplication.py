#
# gqueue
# Next-generation job script templating
#
# NewApplication
# A concrete Application subclass for use with gqueue.
#
# Copyright (c) 2019
# Dr. J Frey, University of Delaware
#

from gqueue.Resource import *
from gqueue.Application import Application


class WrapperApplication(Application):

    @classmethod
    def descriptionString(cls):
        return 'Simple script-wrapper application'

    def jobScriptTemplateForSchedulerAndResources(self, scheduler = None, resources = None):
        return ''
