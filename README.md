# gqueue

Once upon a time, when I was in graduate school and began using the Gaussian quantum chemistry suite, we ran our jobs on a gaggle of IRIX workstations scattered around the lab.  Each run was prefaced by our searching-out the workstation that no one was using, followed by executing the job (via `nohup` and in the background).

When the Beowulf revolution came upon us and we eventually purchased a cluster, the idea of writing job scripts around our Gaussian runs to submit them to a (Grid Engine) queue was very foreign.  The Bash code around the usual `g03 job.com` invocation was always the same, and all that seemed to vary was the processor count and memory limit the job required.  It was perfect fodder for a templating system.

The piece of software became known as **gqueue** — unremarkable shorthand for *submit Gaussian job to queue*.  It used fancy XML configuration files, was written in C, and used external helper programs (Perl scripts, etc.) to parse the input file for required resource limits and to rewrite and merge the Gaussian input into the generated job script.  The utility was written very specifically to use the Grid Engine job scheduling system.  It has survived relatively unaltered on many of the Engineering clusters at the University of Delaware as well as the Mills and Farber clusters (which also use Grid Engine).

A move to Slurm on the University's latest cluster meant that **gqueue** had finally become unusable.  Rather than hacking at the old C code to produce a variant tailored to Slurm, a fresh start seemed appropriate.

## Restructuring

The latest incarnation of **gqueue** is written in Python.  It implements a `Resource` class cluster containing classes that represent the typical types:

- string value
- integer value
- floating-point value
- memory size (with optional unit)
- time duration (1-second resolution)

A collection of named resource values exists as a Python dictionary.  Such dictionaries are modified by subclasses of the `ResourceProcessor` abstract base class.  There are two concrete subclasses that are direct children of `ResourceProcessor`:

- `Application`
- `Scheduler`

Subclasses of the `Scheduler` class provide the detailed behavior for any job scheduler supported by **gqueue**, while subclasses of `Application` handle the custom behavior of individual kinds of jobs.  For the previous version of **gqueue**, the *Scheduler* was `GridEngine` and the *Application* was `Gaussian`.

### Schedulers

Each concrete subclass of `Scheduler` can supply default resource values that form the baseline set of resources for any job processed.  Without any such values, the resources start as an empty dictionary.  Subclasses also have resource filter functions that can view and possibly alter the resource dictionary at key times in the resource-processing pipeline.

Each subclass can register additional resources that are not covered by the default set:

| Name | Description |
| ---- | ----------- |
| `ncpu` | total number of CPUs to request |
| `ncpu-per-node` | number of CPUs to request per node |
| `nnode` | number of nodes to request |
| `mem` | amount of memory per node to request |
| `mem-per-cpu` | amount of memory to request per CPU |
| `time` | maximum wall time limit to request |

Each subclass is naturally reponsible for implementing a job submission method which hands the processes job off to the scheduler it represents.

### Applications

Each concrete subclass of `Application` can supply default resource values that augment the baseline set of resources for any job processed.  Subclasses also have resource filter functions that can view and possibly alter the resource dictionary at key times in the resource processing pipeline.

Each subclass can register additional resources.  For example, the `GaussianApplication` class responds to `--gaussian-logfile` and `--gaussian-version` flags.

## Resource-processing pipeline

Requested resources for the job are processed in the following sequence:

1. Empty dictionary
2. Augment with defaults from chosen `Scheduler` subclass
3. Augment with defaults from chosen `Application` subclass
4. `Scheduler` subclass pre-filter
5. `Application` subclass pre-filter
6. Augment with command line arguments parsed by `Scheduler` subclass
7. Augment with command line arguments parsed by `Application` subclass
8. `Application` subclass post-filter
9. `Scheduler` subclass post-filter

For the `GaussianApplication` subclass, stage 7 includes the import of the Gaussian input file to check for `%nprocshared` and `%mem` directives.  If found, those values augment the resource dictionary passing through the pipeline.  Stage 8 ensures the use of the `ncpu` resource with `nnode` equal to 1, since we do not run Gaussian across multiple nodes.

## Templating

Once resource processing is completed, the `Application` subclass is responsible for generating a job script to be submitted by the `Scheduler`.  The templating engine is an isolated Python namespace with three initial global variables:

- `SCHEDULER` is the instance of the subclass of `Scheduler` being used
- `APPLICATION` is the instance of the subclass of `Application` being used
- `RESOURCES` is the dictionary produced by the resource-processing pipeline

The template is read into memory and each block of text delimited by `[{%` and `%}]` is extracted and executed in its own isolated Python namespace.  No state is retained between the blocks.  Whatever output a block sends to stdout, it replaces the code block.  The template is not recursively processed:  a code block that produces text containing the delimeters is not re-processed.

Code blocks that span whole lines — including multiple lines – are handled specially:  if the code block produces no output to stdout, then the block and the trailing whitespace (including the newline) are removed.  This allows comment blocks to be uninterrupted by whitespace, for example.

## Submission

The generated job script is submitted by the `Scheduler` object in use.  Any output from a sucessful submission is displayed to stdout:

```
$ gqueue --application=Gaussian --extra-argument=--account=it_nss --ncpu=2 ~/h2o.com
Submitted batch job 331083
```

Error messages are written to stderr and a non-zero return code passed back to the caller.

## Environment variables and path searching

The execution of **gqueue** can be effected by several environment variables:

| Variable | Description |
| -------- | ----------- |
| `GQUEUE_PREFIX` | Directory in which the **gqueue** package has been installed (e.g. an alternate root)|
| `GQUEUE_SYSCONFDIR` | Directory containing additional `Application` subclasses and templates; if not set and `GQUEUE_PREFIX` is set, then defaults to `${GQUEUE_PREFIX}/etc` |
| `GQUEUE_SCHEDULER` | Default scheduler to instantiate (currently `Slurm` or `GridEngine` |
| `GQUEUE_APPLICATION` | Application to instantiate by default |
| `GQUEUE_APPLICATION_ADDONS` | PATH-like list of directories to check for `Application` subclasses |
| `GQUEUE_TEMPLATES` | PATH-link list of directories to check for template files |

Individual users can set `GQUEUE_APPLICATION_ADDONS` and provide their own subclasses of `Application` — or even override an existing subclass.  The paths in `GQUEUE_APPLICATION_ADDONS` are consulted in sequence; next `${GQUEUE_SYSCONFDIR}/applications` is consulted if it exists, otherwise `/etc/gqueue/applications` will be checked if it exists.  If all those paths fail, a class in the module itself is the last resort.

Template searches proceed in similar fashion, running through extant `GQUEUE_TEMPLATES` paths, then `${GQUEUE_SYSCONFDIR}/templates` or `/etc/gqueue/templates` if they exist.  Finally, the `Templates` directory inside the  module is checked.

## External Application subclasses

The [external-applications directory](./external-applications) contains a template and a semi-concrete implementation of an external – as in not contained in the **gqueue** module – `Application` subclass.
